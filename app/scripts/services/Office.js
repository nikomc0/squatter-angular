(function () {
	function Office($firebaseArray) {
		var ref = firebase.database().ref().child('offices');
		var offices = $firebaseArray(ref);
		
		return {
			all: offices,
			find: function (location) {
				return $firebaseArray(ref.orderByChild('location').equalTo(location));
			}
		};
	}
	angular
		.module ('squatter')
		.factory ('Office', ['$firebaseArray', Office])
})();