(function () {
	function HomeCtrl (Office) {
		this.offices = Office.all;
		console.log(this.offices);

		this.setLocation = function (location) {
			this.availOffices = Office.find(location);
			return this.availOffices;
		};
	}
	angular
		.module('squatter')
		.controller('HomeCtrl', ['Office', HomeCtrl]);
})();